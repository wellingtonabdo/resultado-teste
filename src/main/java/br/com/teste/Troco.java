package br.com.teste;

import java.util.*;

/**
 * Created by wellington on 01/11/17.
 */
public class Troco {

    public Integer[] buscarNotasParaTrocoPriorizandoMaiorNota(Integer[] notasDisponiveis, Integer troco) throws Exception {
        validarEntradaDados(troco);
        if (notasDisponiveis == null || notasDisponiveis.length == 0) {
            return new Integer[]{};
        }
        Integer[] notasOrdenadas = ordenarNotas(notasDisponiveis);
        List<Integer> notasParaTroco = new ArrayList();
        buscarNotasParaTroco(notasParaTroco, notasOrdenadas, troco);
        Integer[] retorno = new Integer[]{notasParaTroco.size()};
        return notasParaTroco.toArray(retorno);
    }

    private void validarEntradaDados(Integer troco) throws Exception {
        if (troco == null || troco < 0) {
            throw new Exception("O troco não pode ser negativo");
        }
    }

    private void buscarNotasParaTroco(List<Integer> notasParaTroco, Integer[] notas, Integer trocoRestante) {
        Integer maiorNotaPossivel = null;
        for (Integer nota : notas) {
            if (nota <= trocoRestante) {
                maiorNotaPossivel = nota;
                break;
            }
        }
        if (maiorNotaPossivel != null) {
            notasParaTroco.add(maiorNotaPossivel);
            Integer restante = trocoRestante - maiorNotaPossivel;
            if (restante > 0) {
                buscarNotasParaTroco(notasParaTroco, notas, restante);
            }
        }
    }

    public Integer[] ordenarNotas(Integer[] notas) {
        List<Integer> notasOrdenadas = Arrays.asList(notas);
        Collections.sort(notasOrdenadas, new Comparator<Integer>() {
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });
        Integer[] retorno = new Integer[]{notasOrdenadas.size()};
        return notasOrdenadas.toArray(retorno);
    }
}
