package br.com.teste;

/**
 * Created by wellington on 01/11/17.
 */
public class EscadaCaracter {

    public String imprimirEscada(Integer quantidade, char caracter) throws Exception {
        validarEntradaDeDados(quantidade);

        StringBuilder escada = new StringBuilder();
        String quebraLinha = "";
        for (int i = 0; i < quantidade; i++) {
            String degrau = "";
            for (int j = 0; j <= i; j++) {
                degrau += caracter;
            }
            for(int k = degrau.length(); k < quantidade; k++){
                degrau = " " + degrau;
            }
            escada.append(quebraLinha).append(degrau);
            quebraLinha = "\n";
        }
        System.out.println(escada.toString());
        return escada.toString();
    }

    private static void validarEntradaDeDados(Integer quantidade) throws Exception {
        if (quantidade == null || quantidade <= 2) {
            throw new Exception("Quantidade deve ser superior a 2.");
        }
    }
}
