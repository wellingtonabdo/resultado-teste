package br.com.teste;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wellington on 01/11/17.
 */
public class OcorrenciaDuplicada {

    public Integer buscarPrimeiroElementoDuplicado(Integer[] valores) throws Exception {
        validarEntradaDados(valores);

        List<Integer> valoresVerificados = new ArrayList();

        for (Integer valor : valores) {
            if (valoresVerificados.contains(valor)) {
                return valor;
            } else {
                valoresVerificados.add(valor);
            }
        }

        return -1;
    }

    private void validarEntradaDados(Integer[] valores) throws Exception {
        if (valores == null || valores.length == 0) {
            throw new Exception("A lista de valores não pode ser nula ou vazia.");
        } else {
            for (Integer valor : valores) {
                if (valor < 1) {
                    throw new Exception("A lista de valores possui elemento com valor inferior a 1.");
                }
            }
        }
    }
}
