package br.com.teste;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wellington on 01/11/17.
 */
public class OcorrenciaTexto {

    public Integer buscarQuantidadeOcorrenciaPalavraOrFrase(String palavraOrFrase, String ocorrencia) throws Exception {
        validarEntradaDados(palavraOrFrase, ocorrencia);
        List<String> palavras = quebrarFraseListaPalavras(palavraOrFrase);
        Integer quantidadeOcorrencias = 0;
        for (String palavraParaVerificar : palavras) {
            if (palavraParaVerificar.contains(ocorrencia)) {
                quantidadeOcorrencias += 1;
            }
        }
        return quantidadeOcorrencias;
    }

    private List<String> quebrarFraseListaPalavras(String palavraOrFrase) {
        List<String> palavras = new ArrayList();
        String palavra = "";
        for (int i = 0; i < palavraOrFrase.length(); i++) {
            if (palavraOrFrase.charAt(i) == ' ') {
                palavras.add(palavra);
                palavra = "";
            } else {
                palavra += palavraOrFrase.charAt(i);
            }
        }
        palavras.add(palavra);
        return palavras;
    }

    private void validarEntradaDados(String palavraOrFrase, String ocorrencia) throws Exception {
        if(ocorrencia == null || ocorrencia.isEmpty() || palavraOrFrase == null || palavraOrFrase.isEmpty()){
            throw new Exception("A ocorrência não pode ser vazia.");
        }
    }
}
