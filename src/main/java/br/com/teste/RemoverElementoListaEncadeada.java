package br.com.teste;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wellington on 01/11/17.
 */
public class RemoverElementoListaEncadeada {

    public Integer[] criarAndRemoverElementoListaEncadeada(Integer[] valores, Integer valorParaRemover) throws Exception {
        validarEntradaDados(valores);
        Elemento inicio = criarListaEncadeada(valores);
        if (valorParaRemover != null) {
            inicio = removerElementoListaEncadeada(inicio, valorParaRemover);
        }
        Integer[] retorno = criarArrayAPartirDaListaEncadeada(inicio);

        return retorno;
    }

    private Elemento criarListaEncadeada(Integer[] valores) {
        Elemento inicio = new Elemento(null, valores[0]);
        Elemento atual = inicio;
        for (int i = 1; i < valores.length; i++) {
            atual.setProximo(new Elemento(null, valores[i]));
            atual = atual.getProximo();
        }
        return inicio;
    }

    private Elemento removerElementoListaEncadeada(Elemento inicio, Integer valorParaRemover) {
        if (inicio.getValor().equals(valorParaRemover)) {
            inicio = inicio.getProximo();
        } else {
            Elemento atual = inicio;
            while (atual != null && atual.getProximo() != null) {
                if (atual.getProximo().getValor().equals(valorParaRemover)) {
                    atual.setProximo(atual.getProximo().getProximo());
                    atual = null;
                } else {
                    atual = atual.getProximo();
                }
            }
        }
        return inicio;
    }

    public Integer[] criarArrayAPartirDaListaEncadeada(Elemento elemento) {
        List<Integer> valores = new ArrayList<Integer>();
        valores.add(elemento.getValor());
        while (elemento.getProximo() != null) {
            elemento = elemento.getProximo();
            valores.add(elemento.getValor());
        }
        Integer[] retorno = new Integer[]{valores.size()};
        return valores.toArray(retorno);
    }

    public void validarEntradaDados(Integer[] valores) throws Exception {
        if (valores == null || valores.length == 0) {
            throw new Exception("A lista de valores não pode ser vazia.");
        }
    }

    public static class Elemento {
        private Elemento proximo;
        private Integer valor;

        public Elemento(Elemento proximo, Integer valor) {
            this.proximo = proximo;
            this.valor = valor;
        }

        public Integer getValor() {
            return valor;
        }

        public void setValor(Integer valor) {
            this.valor = valor;
        }

        public Elemento getProximo() {
            return proximo;
        }

        public void setProximo(Elemento proximo) {
            this.proximo = proximo;
        }
    }
}
