package br.com.teste;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wellington on 02/11/17.
 */
public class EscadaCaracterTest {

    @Test
    public void imprimirEscadaTest() throws Exception {
        StringBuilder resultadoEsperado = new StringBuilder();
        resultadoEsperado.append("  #").append("\n");
        resultadoEsperado.append(" ##").append("\n");
        resultadoEsperado.append("###");

        EscadaCaracter escadaCaracter = new EscadaCaracter();
        String resultado = escadaCaracter.imprimirEscada(3, '#');

        Assert.assertEquals("Escada de #", resultadoEsperado.toString(), resultado);
    }

    @Test(expected = Exception.class)
    public void imprimirEscadaIgualDoisDegrausTest() throws Exception {
        EscadaCaracter escadaCaracter = new EscadaCaracter();
        escadaCaracter.imprimirEscada(2, '#');
    }
}
