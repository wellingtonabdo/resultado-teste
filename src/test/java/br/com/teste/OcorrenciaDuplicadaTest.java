package br.com.teste;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wellington on 02/11/17.
 */
public class OcorrenciaDuplicadaTest {

    @Test
    public void indicePrimeiraOcorrenciaDuplicadaTest() throws Exception {
        Integer resultadoEsperado = 3;

        OcorrenciaDuplicada ocorrenciaDuplicada = new OcorrenciaDuplicada();
        Integer resultado = ocorrenciaDuplicada.buscarPrimeiroElementoDuplicado(new Integer[]{2, 3, 3, 1, 5, 2});

        Assert.assertEquals("Primeira ocorrência duplicada", resultadoEsperado, resultado);
    }

    @Test
    public void ocorrenciaDupliadaNaoEncontradaTest() throws Exception {
        Integer resultadoEsperado = -1;

        OcorrenciaDuplicada ocorrenciaDuplicada = new OcorrenciaDuplicada();
        Integer resultado = ocorrenciaDuplicada.buscarPrimeiroElementoDuplicado(new Integer[]{2, 3, 4, 1, 5, 6});

        Assert.assertEquals("Ocorrência duplicada não encontrada", resultadoEsperado, resultado);
    }


    @Test(expected = Exception.class)
    public void parametroValoresNullTest() throws Exception {
        OcorrenciaDuplicada ocorrenciaDuplicada = new OcorrenciaDuplicada();
        ocorrenciaDuplicada.buscarPrimeiroElementoDuplicado(null);
    }

    @Test(expected = Exception.class)
    public void parametroValoresVazioTest() throws Exception {
        OcorrenciaDuplicada ocorrenciaDuplicada = new OcorrenciaDuplicada();
        ocorrenciaDuplicada.buscarPrimeiroElementoDuplicado(new Integer[]{});
    }

}
