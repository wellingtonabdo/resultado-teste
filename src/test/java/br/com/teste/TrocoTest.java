package br.com.teste;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wellington on 02/11/17.
 */
public class TrocoTest {

    @Test
    public void buscarNotasParaTrocoPriorizandoMaiorNotaTest() throws Exception {
        Integer[] resultadoEsperado = new Integer[]{50, 20};

        Troco troco = new Troco();
        Integer[] resultado = troco.buscarNotasParaTrocoPriorizandoMaiorNota(new Integer[]{100, 50, 20, 10, 5, 2}, 70);

        Assert.assertArrayEquals("Notas para troco", resultadoEsperado, resultado);
    }

    @Test(expected = Exception.class)
    public void parametrosTrocoNullTest() throws Exception {
        Troco troco = new Troco();
        troco.buscarNotasParaTrocoPriorizandoMaiorNota(new Integer[]{100, 50, 20, 10, 5, 2}, null);
    }

    @Test(expected = Exception.class)
    public void parametrosTrocoNegativoTest() throws Exception {
        Troco troco = new Troco();
        troco.buscarNotasParaTrocoPriorizandoMaiorNota(new Integer[]{100, 50, 20, 10, 5, 2}, -70);
    }
}
