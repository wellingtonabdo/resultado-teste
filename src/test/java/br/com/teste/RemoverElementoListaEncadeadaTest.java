package br.com.teste;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wellington on 02/11/17.
 */
public class RemoverElementoListaEncadeadaTest {

    @Test
    public void removerElementoExistenteListaEncadeadaTest() throws Exception {
        Integer[] resultadoEsperado = new Integer[]{3, 8, 9, 7, 1, 6};

        RemoverElementoListaEncadeada removerElementoListaEncadeada = new RemoverElementoListaEncadeada();
        Integer[] resultado = removerElementoListaEncadeada.criarAndRemoverElementoListaEncadeada(
                new Integer[]{3, 8, 9, 7, 2, 1, 6}, 2);

        Assert.assertArrayEquals("Remover elemento lista encadeada", resultadoEsperado, resultado);
    }


    @Test
    public void removerElementoNaoExistenteListaEncadeadaTest() throws Exception {
        Integer[] resultadoEsperado = new Integer[]{3, 8, 9, 7, 2, 1, 6};

        RemoverElementoListaEncadeada removerElementoListaEncadeada = new RemoverElementoListaEncadeada();
        Integer[] resultado = removerElementoListaEncadeada.criarAndRemoverElementoListaEncadeada(
                new Integer[]{3, 8, 9, 7, 2, 1, 6}, 4);

        Assert.assertArrayEquals("Remover elemento lista encadeada", resultadoEsperado, resultado);
    }

    @Test(expected = Exception.class)
    public void parametroValoresNullTest() throws Exception {
        RemoverElementoListaEncadeada removerElementoListaEncadeada = new RemoverElementoListaEncadeada();
        removerElementoListaEncadeada.criarAndRemoverElementoListaEncadeada(null,2);
    }

    @Test(expected = Exception.class)
    public void parametrosValoresVazioTest() throws Exception {
        RemoverElementoListaEncadeada removerElementoListaEncadeada = new RemoverElementoListaEncadeada();
        removerElementoListaEncadeada.criarAndRemoverElementoListaEncadeada(new Integer[]{}, 2);
    }
}
