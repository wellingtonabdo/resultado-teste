package br.com.teste;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wellington on 02/11/17.
 */
public class OcorrenciaTextoTest {

    @Test
    public void buscarQuantidadeOcorrenciaPalavraOrFraseTest() throws Exception {
        Integer resultadoEsperado = 2;

        OcorrenciaTexto ocorrenciaTexto = new OcorrenciaTexto();
        Integer resultado = ocorrenciaTexto.buscarQuantidadeOcorrenciaPalavraOrFrase(
                "Programador Programação",
                "ama");

        Assert.assertEquals("Quantidade ocorrência(s)", resultadoEsperado, resultado);
    }

    @Test
    public void quantidadeZeroOcorrenciaPalavraOrFraseTest() throws Exception {
        Integer resultadoEsperado = 0;

        OcorrenciaTexto ocorrenciaTexto = new OcorrenciaTexto();
        Integer resultado = ocorrenciaTexto.buscarQuantidadeOcorrenciaPalavraOrFrase(
                "Programador Programação",
                "teste");

        Assert.assertEquals("Quantidade ocorrência(s)", resultadoEsperado, resultado);
    }

    @Test(expected = Exception.class)
    public void parametroPalavraOrFraseNullTest() throws Exception {
        OcorrenciaTexto ocorrenciaTexto = new OcorrenciaTexto();
        ocorrenciaTexto.buscarQuantidadeOcorrenciaPalavraOrFrase(
                null,
                "ama");
    }

    @Test(expected = Exception.class)
    public void parametroOcorrenciaNullTest() throws Exception {
        OcorrenciaTexto ocorrenciaTexto = new OcorrenciaTexto();
        ocorrenciaTexto.buscarQuantidadeOcorrenciaPalavraOrFrase(
                "333",
                null);
    }

    @Test(expected = Exception.class)
    public void parametroPalavraOrFraseVazioTest() throws Exception {
        OcorrenciaTexto ocorrenciaTexto = new OcorrenciaTexto();
        ocorrenciaTexto.buscarQuantidadeOcorrenciaPalavraOrFrase(
                "",
                "ama");
    }

    @Test(expected = Exception.class)
    public void parametroOcorrenciaVazioTest() throws Exception {
        OcorrenciaTexto ocorrenciaTexto = new OcorrenciaTexto();
        ocorrenciaTexto.buscarQuantidadeOcorrenciaPalavraOrFrase(
                "333",
                "");
    }
}
